--------------------------------------------------------
-- Minetest :: Emitblock Mod v1.0 (emitblock)
--
-- See README.txt for licensing and other information.
-- Copyright (c) 2017-2020, Leslie E. Krause
--------------------------------------------------------

minetest.load_config( )

minetest.register_node( ":mobs:emitblock", {
        description = "Mob Spawner",
        drawtype = "glasslike",
        tiles = { "teleports_teleport_top.png" },
        is_ground_content = false,
        groups = { cracky = 1, level = 3 },
        sounds = default.node_sound_stone_defaults( ),

	can_dig = function( pos, player )
		return  default.is_owner( pos, player:get_player_name( ) )
	end,
	on_rightclick = function( pos, node, player )
		if default.is_owner( pos, player:get_player_name( ) ) then
			local emit_defs = config.emit_defs
			local index = minetest.get_meta( pos ):get_int( "index" )
			local chance = minetest.get_meta( pos ):get_int( "chance" )
			local is_nocturnal

			if index == 0 then
				minetest.get_node_timer( pos ):start( 15 )
			end

			chance = index == #emit_defs and chance + 5 or chance
			index = index == #emit_defs and 1 or index + 1
			is_nocturnal = minetest.registered_entities[ emit_defs[ index ] ].light_damage > 0 and pos.y > 0

			minetest.get_meta( pos ):set_int( "index", index )
			minetest.get_meta( pos ):set_int( "chance", chance )
			minetest.chat_send_player( player:get_player_name( ), string.format( "Emitter set to %s with 1 in %d chance of firing %s.",
				emit_defs[ index ], chance, is_nocturnal == false and "anytime" or "at night" )
			)
		end
	end,
	on_timer = function( pos, elapsed )
		local localtime = minetest.get_timeofday( )
		local index = minetest.get_meta( pos ):get_int( "index" )
		local chance = minetest.get_meta( pos ):get_int( "chance" )
		local emit_defs = config.emit_defs

		if index == 0 or not emit_defs[ index ] then
			return false   -- abort on invalid emitter index
		end

		local is_nocturnal = minetest.registered_entities[ emit_defs[ index ] ].light_damage > 0 and pos.y > 0
		local y_offset = minetest.registered_entities[ emit_defs[ index ] ].y_offset

		if math.random( chance ) == 1 and ( is_nocturnal == false or localtime <= 0.2 or localtime >= 0.8 ) then
			minetest.add_entity( { x = pos.x, y = pos.y + y_offset, z = pos.z }, emit_defs[ index ] )
			minetest.log( "action", "Adding mob " .. emit_defs[ index ] .. " on block mobs:emitblock at " .. minetest.pos_to_string( pos ) )
		end
		return true
	end,
	after_place_node = function( pos, player, itemstack )
		local pname = player:get_player_name( )
		local meta = minetest.get_meta( pos )

		meta:set_int( "index", 0 )
		meta:set_int( "chance", 5 )

		meta:set_string( "infotext", "Mob Spawner (Owned by " .. pname .. ")" )
		meta:set_string( "owner", pname )
        end,
} )

minetest.register_node( ":mobs:phoenix_spawner", {
        description = "Phoenix Spawner",
        drawtype = "glasslike",
        tiles = { "teleports_teleport_top.png" },
        is_ground_content = false,
        groups = { cracky = 1, level = 3 },
        sounds = default.node_sound_stone_defaults( ),

        can_dig = function( pos, player )
                return  default.is_owner( pos, player:get_player_name( ) )
        end,
        on_rightclick = function( pos, node, player )
                if  default.is_owner( pos, player:get_player_name( ) ) then
                        minetest.add_entity( { x = pos.x + math.random( -5, 5 ), y = pos.y + 15, z = pos.z + math.random( -5, 5 ) }, "mobs:phoenix" )

                        print( "Spawning a phoenix at ", minetest.pos_to_string( pos ) )
                        minetest.chat_send_all( "A deadly Phoenix is heading for the city. Be on the lookout!" )
                end
        end,
        on_timer = function( pos, elapsed )
--		local localtime = minetest.get_timeofday( )
--		if math.random( 5 ) == 1 and ( localtime <= 0.2 or localtime >= 0.8 ) then
--			minetest.add_entity( { x = pos.x + math.random( -5, 5 ), y = pos.y + 15, z = pos.z + math.random( -5, 5 ) }, "mobs:phoenix" )
--
--			minetest.log( "action", "Adding mob mobs:phoenix on block mobs:phoenix_spawner at " .. minetest.pos_to_string( pos ) )
--			minetest.chat_send_all( "A deadly Phoenix is heading for the city. Be on the lookout!" )
--		end
		return true
        end,
        after_place_node = function( pos, placer, itemstack )
                local pname = placer:get_player_name( )
                local meta = minetest.get_meta( pos )

                meta:set_string( "infotext", "Phoenix Spawner (Owned by " .. pname .. ")" )
                meta:set_string( "owner", pname )
                minetest.get_node_timer( pos ):start( 60 )
        end,
} )
